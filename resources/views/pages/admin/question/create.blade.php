@extends ('layouts.admin.app')

@section('title', 'Tambah Pertanyaan')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 pt-3">
        <h1 class="h3 mb-0 text-gray-800">Tambah Pertanyaan</h1> 
    </div>

    <!-- Munculkan error -->
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach        
        </ul>
    </div>
    @endif

  <!-- Card Form tambah berita -->
  <div class="card shadow">
    <div class="card-body">
        <form action="{{ route('QuestionStore') }}" method="POST">
         @csrf         
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" placeholder="judul" value="{{ old('judul') }}">
            </div> 
            <div class="form-group">
                <label for="isi">Pertanyaan</label>
                <textarea name="isi" rows="10" class="d-block w-100 form-control">{{ old('isi') }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Kirim</button>
        </form>
     </div>
    </div>
</div>
@endsection