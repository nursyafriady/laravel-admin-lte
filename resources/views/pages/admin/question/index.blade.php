@extends ('layouts.admin.app')

@section('title', 'Daftar Pertanyaan')

@section ('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between pt-4 mb-2">
        <h1 class="h3 mb-0 ml-3 text-gray-800">Daftar Pertanyaan</h1>
        <a href=" {{ route('QuestionCreate') }}" class="btn btn-sm btn-primary shadow-sm mr-3">
            <i class="fas fa-plus fa-sm text-white-100"></i> Tambah Pertanyaan
        </a>
    </div>

    <div class="row">
        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">           
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('success') }}
            </div>
            @endif
            
            <div class="table-responsive">
                <table id="tableQuestion" class="table table-bordered table-hover" width="100%" cellspacing="0" data-form="deleteForm">
                    <thead class="thead-dark">
                        <tr>
                            <th style="width: 5%;">No.</th>                          
                            <th>Judul</th> 
                            <th style="width: 50%;">Pertanyaan</th>                 
                            <th style="width: 15%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $no = 0;?>
                    @forelse ($questions as $question)
                    <?php $no++ ;?>
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ $question->judul }}</td>
                            <td>{{ $question->isi }}</td>                  
                            <td>
                                <a href="{{ route('QuestionShow', $question->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="detail">
                                    <i class="fas fa-eye"></i>                           
                                </a>
                                <a href="{{ route('QuestionEdit', $question->id) }}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="edit">
                                    <i class="fa fa-pencil-alt"></i>                            
                                </a>  
                                <form action="{{ route('QuestionDelete', $question->id) }}"
                                    method="POST" class="d-inline form-delete">
                                    @csrf 
                                    @method('delete')
                                    <button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="delete">
                                        <i class="fa fa-trash" ></i>
                                    </button>
                                </form>                              
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" class="text-center">
                                Belum Ada Pertanyaan
                            </td>
                        </tr>                    
                    @endforelse                     
                    </tbody>
                </table>            
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@include('includes.admin.modal')
@endsection

@push('after-scripts')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js ') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js ') }}"></script>
    <script>
    $(function () {
        $("#tableQuestion").DataTable();
    });
    </script>
@endpush

