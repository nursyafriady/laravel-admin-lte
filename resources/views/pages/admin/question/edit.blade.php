@extends ('layouts.admin.app')

@section('title', 'Ubah Pertanyaan')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 pt-3">
        <h1 class="h3 mb-0 text-gray-800">Ubah Pertanyaan <strong>{{ $questions->judul }}</strong> </h1> 
    </div>

    <!-- Munculkan error -->
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach        
        </ul>
    </div>
    @endif

  <!-- Card Form tambah berita -->
  <div class="card shadow">
    <div class="card-body">
        <form action="{{ route('QuestionPut', $questions->id) }}" method="POST">
        @method('PUT')
         @csrf         
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" placeholder="judul" value="{{ $questions->judul }}">
            </div> 
            <div class="form-group">
                <label for="isi">Pertanyaan</label>
                <textarea name="isi" rows="10" class="d-block w-100 form-control">{{ $questions->isi }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Ubah</button>
        </form>
     </div>
    </div>
</div>
@endsection