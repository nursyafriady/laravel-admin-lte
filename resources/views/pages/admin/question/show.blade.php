@extends ('layouts.admin.app')

@section('title', 'Detail Pertanyaan')

@section('content')
    <div class="card p-5">
        <div class="card-header">
            <strong>Detail Pertanyaan</strong> 
        </div>
        <div class="card-body">
            <h2 class="card-title mb-2"> 
                <strong>
                    Judul : {{ $questions->judul }}
                </strong>           
            </h2>
            <p class="card-text">{{ $questions->isi }}</p>
            <a href="{{ route('question') }}" class="btn btn-primary mt-3">Kembali</a>
        </div>
    </div>
@endsection


