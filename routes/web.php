<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/question', 'QuestionController@index')
        ->name('question');
Route::get('/question/create', 'QuestionController@create')
        ->name('QuestionCreate');
Route::post('/question', 'QuestionController@store')
        ->name('QuestionStore');
Route::get('/question/{id}', 'QuestionController@show')
        ->name('QuestionShow');
Route::get('/question/{id}/edit', 'QuestionController@edit')
        ->name('QuestionEdit');
Route::put('/question/{id}', 'QuestionController@update')
        ->name('QuestionPut');
Route::delete('/question/{id}', 'QuestionController@destroy')
        ->name('QuestionDelete');

// Route::get('/', 'HomeController@index')
//         ->name('home');
// Route::get('/data-tables', 'DataTablesController@index')
//         ->name('dataTables');

// Route::get('/register', 'AuthController@register')
//         ->name('register');
// Route::get('/welcome', 'AuthController@welcome')
//         ->name('welcome');
// Route::post('/welcome', 'AuthController@welcome')
//         ->name('welcome');
// Route::resource('/admin', Admin\DashboardController::class);

// Route::get('/admin', function () {
//     return view('layouts.admin.app');
// });

// Route::get('/', function () {
//     return view('welcome');
// });